package br.com.test.compasso.uol.api.service;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import br.com.test.compasso.uol.api.component.ProductAssembler;
import br.com.test.compasso.uol.api.entity.Product;
import br.com.test.compasso.uol.api.exception.ProductException;
import br.com.test.compasso.uol.api.model.ProductRequestDTO;
import br.com.test.compasso.uol.api.model.ProductResponseDTO;
import br.com.test.compasso.uol.api.repository.ProductRepository;
import br.com.test.compasso.uol.api.specs.ProductSpecification;
import br.com.test.compasso.uol.api.specs.SearchCriteria;
import br.com.test.compasso.uol.api.specs.SearchOperation;

@Service
public class ProductService {

	@Autowired
	private ProductRepository productRepository;

	@Autowired
	private ProductAssembler productAssembler;
	
	private static final String NAME="name";
	private static final String PRICE="price";
	private static final String DESCRIPTION="description";

	public ProductResponseDTO createProduct(ProductRequestDTO dto) {

		Product p = productAssembler.productCreateFromDTO(dto);

		return productAssembler.productResponseFromEntity(productRepository.save(p));
	}

	public ProductResponseDTO updateProduct(ProductRequestDTO request, Long id) throws Exception {

		Product p = getProductBydId(id);

		p.setName(request.getName());
		p.setDescription(request.getDescription());
		p.setPrice(request.getPrice());

		return productAssembler.productResponseFromEntity(productRepository.save(p));
	}

	public ProductResponseDTO productSearch(Long id) throws ProductException {

		Product p = getProductBydId(id);

		return productAssembler.productResponseFromEntity(p);
	}

	private Product getProductBydId(Long id) throws ProductException {

		Optional<Product> pExists = productRepository.findById(id);
		if (pExists.isEmpty())
			throw new ProductException("Produto não localizado");
		return pExists.get();
	}

	public   List<ProductResponseDTO> getAllProducts() {
		return productAssembler.productResponseFromList(productRepository.findAll());
	}

	public void removeProductById(Long id) throws ProductException {
		
		Product p = getProductBydId(id);
		 productRepository.deleteById(p.getId());
	}

	public List<ProductResponseDTO> searchProductByFilter(String q, Double minPrice, Double maxPrice) {
		
		ProductSpecification psName = new ProductSpecification();
		ProductSpecification psDesc = new ProductSpecification();
		ProductSpecification psMinPrice = new ProductSpecification();
		ProductSpecification psMaxPrice = new ProductSpecification();

		
		if (null!=q || q.length() > 0 ) {
			
			psName.add(new SearchCriteria(NAME, q, SearchOperation.MATCH));
			psDesc.add(new SearchCriteria(DESCRIPTION, q, SearchOperation.MATCH));
			
		}if (null!=minPrice) {
			psMinPrice.add(new SearchCriteria(PRICE, minPrice, SearchOperation.GREATER_THAN_EQUAL));

		}
		if (null!=maxPrice) {
			psMaxPrice.add(new SearchCriteria(PRICE, maxPrice, SearchOperation.LESS_THAN_EQUAL));

		}

		return productAssembler.productResponseFromList( productRepository.findAll(Specification.where(psName).or(psDesc).and(psMinPrice).and(psMaxPrice)));
	}

}
