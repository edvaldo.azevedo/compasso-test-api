package br.com.test.compasso.uol.api.component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import br.com.test.compasso.uol.api.entity.Product;
import br.com.test.compasso.uol.api.model.ProductRequestDTO;
import br.com.test.compasso.uol.api.model.ProductResponseDTO;

@Component
public class ProductAssembler {

	
	public Product productCreateFromDTO( ProductRequestDTO dto) {
		
		Product p =  null;
		if (dto!=null) {
			p = Product.builder().build();
			p.setName(dto.getName());
			p.setPrice(dto.getPrice());
			p.setDescription(dto.getDescription());
		}
		return p;
	}
	
	public ProductResponseDTO productResponseFromEntity(Product entity) {
		
		ProductResponseDTO dto = null;
		if (entity!=null) {
			dto = ProductResponseDTO.builder().build();
			dto.setDescription(entity.getDescription());
			dto.setId(entity.getId());
			dto.setName(entity.getName());
			dto.setPrice(entity.getPrice());
		}
		return dto;
	}
	public List<ProductResponseDTO> productResponseFromList(List<Product>list){
		
		List<ProductResponseDTO> response = new ArrayList<ProductResponseDTO>();
		if (!list.isEmpty()) {
			response = list.stream().map(e ->{
				ProductResponseDTO dto = ProductResponseDTO.builder().build();
				dto.setId(e.getId());
				dto.setName(e.getName());
				dto.setPrice(e.getPrice());
				dto.setDescription(e.getDescription());
				return dto;
			}).collect(Collectors.toList());
			
		}
		return response;
	}
}
