package br.com.test.compasso.uol.api.controller;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.test.compasso.uol.api.entity.Product;
import br.com.test.compasso.uol.api.exception.ProductException;
import br.com.test.compasso.uol.api.model.ProductRequestDTO;
import br.com.test.compasso.uol.api.model.ProductResponseDTO;
import br.com.test.compasso.uol.api.service.ProductService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;

@RestController
@RequestMapping("/api/products")
@Tag(description = "enpoint para acesso de recursos product", name = "product-ms")
public class ProductController {

	@Autowired
	private ProductService productService;

	@PostMapping(headers = "Accept=application/json")
	@Operation(description = "create products")
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<?> createdProduct(@Valid @RequestBody(required = true) ProductRequestDTO request,
			BindingResult result) {

		if (result.hasErrors()) {
			ProductException error = new ProductException(result.getFieldError().getDefaultMessage());
			return ResponseEntity.badRequest().body(error);
		}

		return ResponseEntity.ok(productService.createProduct(request));

	}

	@PutMapping(path = "/{id}")
	@Operation(description = "update product")
	@ResponseStatus(HttpStatus.CREATED)

	public ResponseEntity<?> updateProduct(@Valid @RequestBody(required = true) ProductRequestDTO request,
			@RequestParam(name = "id") Long id, BindingResult result) throws Exception {
		if (result.hasErrors()) {
			List<ProductException> error = result.getFieldErrors().stream()
					.map(e -> new ProductException(e.getDefaultMessage())).collect(Collectors.toList());
			return ResponseEntity.badRequest().body(error);
		}
		return ResponseEntity.ok(productService.updateProduct(request, id));

	}

	@GetMapping(path = "/{id}")
	@Operation(description = " get product")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<?> productSearch(@RequestParam(name = "id", required = true) Long id)
			throws ProductException {

		return ResponseEntity.ok(productService.productSearch(id));

	}

	@GetMapping
	@Operation(description = " get products")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<?> listProducts() {

		return ResponseEntity.ok(productService.getAllProducts());
	}

	@GetMapping(path = "/search")
	public List<ProductResponseDTO> searchProductByFilter(@Parameter @RequestParam(required = false, defaultValue = "") String q,
			@Parameter @RequestParam(required = false ) Double minPrice,
			@Parameter @RequestParam(required = false) Double maxPrice) {
	
		return  productService.searchProductByFilter(q, minPrice,maxPrice);

	}

	@DeleteMapping(path = "/{id}")
	@Operation(description = " delete product")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<?> removeProduct(@RequestParam(name = "id", required = true) Long id) throws ProductException {
		productService.removeProductById(id);
		return ResponseEntity.ok().body(HttpStatus.OK);
	}

}
