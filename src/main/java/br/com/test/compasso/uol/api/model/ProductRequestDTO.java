package br.com.test.compasso.uol.api.model;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class ProductRequestDTO {

	@NotBlank(message = "campo name não pode ser nulo ou vazio")
	private String name;

	@NotBlank(message = "campo description não pode ser nulo ou vazio")
	private String description;

	@NotNull(message = "campo price não pode ser nulo")
	private Double price;

}
