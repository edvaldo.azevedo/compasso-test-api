package br.com.test.compasso.uol.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestCompassoUolApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestCompassoUolApiApplication.class, args);
	}

}
