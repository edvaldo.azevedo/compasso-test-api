package br.com.test.compasso.uol.api.exception;

import org.springframework.validation.FieldError;

import lombok.Data;

@Data
public class ProductException extends Exception {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4030866553590947593L;
	private int statusCode = 400;
	private String message;
	
	public ProductException(String message) {
		
		this.message =message;
		
		
	}


}
