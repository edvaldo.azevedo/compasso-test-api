# Compasso UOL API test
### Aplicação para Test vaga Dev


## Technologies

- SpringBoot
- H2 DB
- OpenApi

## Features

### product-ms

- POST /api/products
- PUT /api/product/{id}
- DELETE /api/products/{id}
- GET /api/products
- GET /api/products/{id}
- GET /api/products/search

## Build

- mvn package

## Run

- ./mvnw spring-boot:run
 
## Swagger UI

Para acessar o Swagger : http://localhost:9999/swagger-ui-custom.html


